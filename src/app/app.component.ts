import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events, MenuController, ModalController, AlertController, App, ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NetworkProvider } from '../providers/network/network';
import { MenuProvider } from '../providers/menu/menu';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import { SideMenuOption } from '../../shared/side-menu-content/models/side-menu-option';
import { SideMenuContentComponent } from '../../shared/side-menu-content/side-menu-content.component';
import { SideMenuSettings } from '../../shared/side-menu-content/models/side-menu-settings';
import { ReplaySubject } from "rxjs/ReplaySubject";
import { ApiServiceProvider } from '../providers/api-service/api-service';
import { TextToSpeech } from '@ionic-native/text-to-speech';
import { AppVersion } from '@ionic-native/app-version';

@Component({
  selector: 'app-main-page',
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // Get the instance to call the public methods
  @ViewChild(SideMenuContentComponent) sideMenu: SideMenuContentComponent;

  rootPage: any;
  pages: any;
  selectedMenu: any;

  islogin: any = {};
  setsmsforotp: string;
  DealerDetails: any;
  dealerStatus: any;
  dealerChk: any;

  // Options to show in the SideMenuContentComponent
  public options: Array<SideMenuOption>;

  // Settings for the SideMenuContentComponent
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option'
  };
  private unreadCountObservable: any = new ReplaySubject<number>(0);
  token: any;
  isDealer: any;
  notfiD: boolean;
  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    // private network: Network,
    public events: Events,
    public networkProvider: NetworkProvider,
    public menuProvider: MenuProvider,
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    private push: Push,
    public alertCtrl: AlertController,
    public app: App,
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    private tts: TextToSpeech,
    public appVersion: AppVersion
  ) {
    // this.backgroundMode.enable();
    this.events.subscribe('user:updated', (udata) => {
      this.islogin = udata;
      this.isDealer = udata.isDealer;
      console.log("islogin=> " + JSON.stringify(this.islogin));
    });

    this.events.subscribe('notif:updated', (notifData) => {
      console.log("text to speech updated=> ", notifData);
      // this.notfiD = notifData;
      localStorage.setItem("notifValue", notifData);
    });

    platform.ready().then(() => {

      statusBar.styleDefault();
      statusBar.hide();
      this.splashScreen.hide();

      platform.registerBackButtonAction(() => {
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
        if (activeView.name == "DashboardPage") {
          const alert = this.alertCtrl.create({
            title: 'App termination',
            message: 'Do you want to close the app?',
            buttons: [{
              text: 'Cancel',
              role: 'cancel',
              handler: () => {
                console.log('Application exit prevented!');
              }
            }, {
              text: 'Close App',
              handler: () => {
                this.platform.exitApp(); // Close this application
              }
            }]
          });
          alert.present();
        } else {
          if (nav.canGoBack()) { // CHECK IF THE USER IS IN THE ROOT PAGE.
            nav.pop(); // IF IT'S NOT THE ROOT, POP A PAGE.
          } else {
            platform.exitApp(); // IF IT'S THE ROOT, EXIT THE APP.
          }
        }
      });
    });

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("islogin=> " + JSON.stringify(this.islogin));

    this.setsmsforotp = localStorage.getItem('setsms');

    this.DealerDetails = JSON.stringify(localStorage.getItem('dealer')) || {};
    console.log("DealerDetails=> " + this.DealerDetails._id);

    this.dealerStatus = this.islogin.isDealer;
    console.log("dealerStatus=> " + this.dealerStatus);

    this.getSideMenuData();
    this.initializeApp();

    if (localStorage.getItem("loginflag")) {
      // debugger;
      if (localStorage.getItem("SCREEN") != null) {
        if (localStorage.getItem("SCREEN") === 'live') {
          this.rootPage = 'LivePage';
        } else {
          if (localStorage.getItem("SCREEN") === 'dashboard') {
            this.rootPage = 'DashboardPage';
          }
        }
      } else {
        this.rootPage = 'DashboardPage';
      }
    } else {
      this.rootPage = 'LoginPage';
    }

  }

  getSideMenuData() {
    this.pages = this.menuProvider.getSideMenus();
  }

  pushNotify() {
    let that = this;
    that.push.hasPermission()                       // to check if we have permission
      .then((res: any) => {

        if (res.isEnabled) {
          console.log('We have permission to send push notifications');
          that.pushSetup();
        } else {
          console.log('We do not have permission to send push notifications');
          that.pushSetup();
        }
      });
  }

  pushSetup() {
    // to initialize push notifications
    let that = this;
    const options: PushOptions = {
      android: {
        senderID: '644983599736',
        // icon: './assets/imgs/yellow-bus-icon-40028.png'
      },
      ios: {
        alert: 'true',
        badge: true,
        sound: 'true'
      },
      windows: {},
      browser: {
        pushServiceURL: 'http://push.api.phonegap.com/v1/push'
      }
    };

    const pushObject: PushObject = that.push.init(options);


    pushObject.on('notification').subscribe((notification: any) => {
      if (localStorage.getItem("notifValue") != null) {
        if (localStorage.getItem("notifValue") == 'true') {
          this.tts.speak(notification.message)
            .then(() => console.log('Success'))
            .catch((reason: any) => console.log(reason));
        }
      }

      if (notification.additionalData.foreground) {
        const toast = this.toastCtrl.create({
          message: notification.message,
          duration: 2000
        });
        toast.present();
      }
    });

    pushObject.on('registration')
      .subscribe((registration: any) => {
        // alert(registration.registrationId)
        console.log("device token => " + registration.registrationId);
        // console.log("reg type=> " + registration.registrationType);
        localStorage.setItem("DEVICE_TOKEN", registration.registrationId);
        // that.storage.set("DEVICE_TOKEN", registration.registrationId);

      });

    pushObject.on('error').subscribe(error => {
      console.error('Error with Push plugin', error)
      // alert('Error with Push plugin' + error)
    });
  }

  initializeApp() {
    let that = this;
    that.platform.ready().then(() => {

      that.pushNotify();

      that.networkProvider.initializeNetworkEvents();

      // Offline event
      that.events.subscribe('network:offline', () => {
        // alert('network:offline ==> ' + this.networkProvider.getNetworkType());
        alert("Internet is not connected... please make sure the internet connection is working properly.")
      });

      // Online event
      that.events.subscribe('network:online', () => {
        alert('network:online ==> ' + this.networkProvider.getNetworkType());
      });

      // that.backBtnHandler();
    });

    // Initialize some options
    that.initializeOptions();
    // Change the value for the batch every 5 seconds
    setInterval(() => {
      this.unreadCountObservable.next(Math.floor(Math.random() * 10) + 1);
    }, 5000);
  }


  private initializeOptions(): void {
    this.options = new Array<SideMenuOption>();
    
    if ((this.islogin.role == undefined || this.islogin.role == 'supAdm' || this.islogin.role == 'adm') && this.islogin.isSuperAdmin == true && this.islogin.isDealer == false) {
      this.options.push({
        iconName: 'home',
        displayText: 'Home',
        component: 'DashboardPage',
      });

      this.options.push({
        iconName: 'people',
        displayText: 'Groups',
        component: 'GroupsPage'
      });

      this.options.push({
        iconName: 'people',
        displayText: 'Dealers',
        component: 'DealerPage'
      });

      this.options.push({
        iconName: 'contacts',
        displayText: 'Customers',
        component: 'CustomersPage'
      });

      this.options.push({
        iconName: 'notifications',
        displayText: 'Notifications',
        component: 'AllNotificationsPage'
      });

      this.options.push({
        iconName: 'walk',
        displayText: 'Your Trips',
        component: 'YourTripsPage'
      });

      this.options.push({
        iconName: 'list',
        displayText: 'POI List',
        component: 'PoiListPage'
      });
      this.options.push({
        displayText: 'Fuel',
        iconName: 'arrow-dropright',
        suboptions: [
          {
            iconName: 'custom-fuel',
            displayText: 'Fuel Consumption',
            component: 'FuelConsumptionPage'
          },
          {
            iconName: 'custom-fuel',
            displayText: 'Fuel Report',
            component: 'FuelConsumptionReportPage'
          },
          {
            iconName: 'custom-fuel',
            displayText: 'Fuel Chart',
            component: 'FuelChartPage'
          }
        ]
      });

      // Load options with nested items (with icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Reports',
        iconName: 'arrow-dropright',
        suboptions: [
          {
            iconName: 'clipboard',
            displayText: 'AC Report',
            component: 'AcReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Daily Report',
            component: 'DailyReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Summary Report',
            component: 'DeviceSummaryRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Geofenceing Report',
            component: 'GeofenceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Overspeed Report',
            component: 'OverSpeedRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Ignition Report',
            component: 'IgnitionReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Stoppage Report',
            component: 'StoppagesRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Distance Report',
            component: 'DistanceReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Trip Report',
            component: 'TripReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Route Violation Report',
            component: 'RouteVoilationsPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'Speed Variation Report',
            component: 'SpeedRepoPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'SOS Report',
            component: 'SosReportPage'
          },
          {
            iconName: 'clipboard',
            displayText: 'POI Report',
            component: 'POIReportPage'
          }
        ]
      });

      // Load options with nested items (without icons)
      // -----------------------------------------------
      this.options.push({
        displayText: 'Routes',
        iconName: 'map',
        component: 'RoutePage'
      });

      // Load special options
      // -----------------------------------------------
      this.options.push({
        displayText: 'Customer Support',
        suboptions: [
          {
            iconName: 'star',
            displayText: 'Rate Us',
            component: 'FeedbackPage'
          },
          {
            iconName: 'mail',
            displayText: 'Contact Us',
            component: 'ContactUsPage'
          }
        ]
      });
      this.options.push({
        displayText: 'Profile',
        iconName: 'person',
        component: 'ProfilePage'
      });
    } else {
      if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == false) {
        this.options.push({
          iconName: 'home',
          displayText: 'Home',
          component: 'DashboardPage',
        });

        this.options.push({
          iconName: 'people',
          displayText: 'Groups',
          component: 'GroupsPage'
        });

        this.options.push({
          iconName: 'notifications',
          displayText: 'Notifications',
          component: 'AllNotificationsPage'
        });
        this.options.push({
          iconName: 'walk',
          displayText: 'Your Trips',
          component: 'YourTripsPage'
        });

        this.options.push({
          iconName: 'list',
          displayText: 'POI List',
          component: 'PoiListPage'
        });

        this.options.push({
          displayText: 'Fuel',
          iconName: 'arrow-dropright',
          suboptions: [
            {
              iconName: 'custom-fuel',
              displayText: 'Fuel Consumption',
              component: 'FuelConsumptionPage'
            },
            {
              iconName: 'custom-fuel',
              displayText: 'Fuel Report',
              component: 'FuelConsumptionReportPage'
            },
            {
              iconName: 'custom-fuel',
              displayText: 'Fuel Chart',
              component: 'FuelChartPage'
            }
          ]
        });

        // Load options with nested items (with icons)
        // -----------------------------------------------
        this.options.push({
          displayText: 'Reports',
          iconName: 'arrow-dropright',
          suboptions: [
            {
              iconName: 'clipboard',
              displayText: 'AC Report',
              component: 'AcReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Daily Report',
              component: 'DailyReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Summary Report',
              component: 'DeviceSummaryRepoPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Geofenceing Report',
              component: 'GeofenceReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Overspeed Report',
              component: 'OverSpeedRepoPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Ignition Report',
              component: 'IgnitionReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Stoppage Report',
              component: 'StoppagesRepoPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Distance Report',
              component: 'DistanceReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Trip Report',
              component: 'TripReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Route Violation Report',
              component: 'RouteVoilationsPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'Speed Variation Report',
              component: 'SpeedRepoPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'SOS Report',
              component: 'SosReportPage'
            },
            {
              iconName: 'clipboard',
              displayText: 'POI Report',
              component: 'POIReportPage'
            }
          ]
        });

        // Load options with nested items (without icons)
        // -----------------------------------------------
        this.options.push({
          displayText: 'Routes',
          iconName: 'map',
          component: 'RoutePage'
        });

        // Load special options
        // -----------------------------------------------
        this.options.push({
          displayText: 'Customer Support',
          suboptions: [
            {
              iconName: 'star',
              displayText: 'Rate Us',
              component: 'FeedbackPage'
            },
            {
              iconName: 'mail',
              displayText: 'Contact Us',
              component: 'ContactUsPage'
            }
          ]
        });
        this.options.push({
          displayText: 'Profile',
          iconName: 'person',
          component: 'ProfilePage'
        });
      } else {
        if (this.islogin.role == undefined && this.islogin.isSuperAdmin == false && this.islogin.isDealer == true) {
          this.options.push({
            iconName: 'home',
            displayText: 'Home',
            component: 'DashboardPage',
          });

          this.options.push({
            iconName: 'people',
            displayText: 'Groups',
            component: 'GroupsPage'
          });

          this.options.push({
            iconName: 'contacts',
            displayText: 'Customers',
            component: 'CustomersPage'
          });
          

          this.options.push({
            iconName: 'notifications',
            displayText: 'Notifications',
            component: 'AllNotificationsPage'
          });
          this.options.push({
            iconName: 'walk',
            displayText: 'Your Trips',
            component: 'YourTripsPage'
          });
          this.options.push({
            iconName: 'list',
            displayText: 'POI List',
            component: 'PoiListPage'
          });
          this.options.push({
            displayText: 'Fuel',
            iconName: 'arrow-dropright',
            suboptions: [
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Consumption',
                component: 'FuelConsumptionPage'
              },
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Report',
                component: 'FuelConsumptionReportPage'
              },
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Chart',
                component: 'FuelChartPage'
              }
            ]
          });
          // Load options with nested items (with icons)
          // -----------------------------------------------
          this.options.push({
            displayText: 'Reports',
            iconName: 'arrow-dropright',
            suboptions: [
              {
                iconName: 'clipboard',
                displayText: 'AC Report',
                component: 'AcReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Daily Report',
                component: 'DailyReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Summary Report',
                component: 'DeviceSummaryRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Geofenceing Report',
                component: 'GeofenceReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Overspeed Report',
                component: 'OverSpeedRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Ignition Report',
                component: 'IgnitionReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Stoppage Report',
                component: 'StoppagesRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Distance Report',
                component: 'DistanceReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Trip Report',
                component: 'TripReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Route Violation Report',
                component: 'RouteVoilationsPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Speed Variation Report',
                component: 'SpeedRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'SOS Report',
                component: 'SosReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'POI Report',
                component: 'POIReportPage'
              }
            ]
          });

          // Load options with nested items (without icons)
          // -----------------------------------------------
          this.options.push({
            displayText: 'Routes',
            iconName: 'map',
            component: 'RoutePage'
          });

          // Load special options
          // -----------------------------------------------
          this.options.push({
            displayText: 'Customer Support',
            suboptions: [
              {
                iconName: 'star',
                displayText: 'Rate Us',
                component: 'FeedbackPage'
              },
              {
                iconName: 'mail',
                displayText: 'Contact Us',
                component: 'ContactUsPage'
              }
            ]
          });
          this.options.push({
            displayText: 'Profile',
            iconName: 'person',
            component: 'ProfilePage'
          });
        } else {
          this.options.push({
            iconName: 'home',
            displayText: 'Home',
            component: 'DashboardPage',
          });

          this.options.push({
            iconName: 'people',
            displayText: 'Groups',
            component: 'GroupsPage'
          });

          this.options.push({
            iconName: 'notifications',
            displayText: 'Notifications',
            component: 'AllNotificationsPage'
          });
          this.options.push({
            iconName: 'walk',
            displayText: 'Your Trips',
            component: 'YourTripsPage'
          });

          this.options.push({
            iconName: 'list',
            displayText: 'POI List',
            component: 'PoiListPage'
          });
          this.options.push({
            displayText: 'Fuel',
            iconName: 'arrow-dropright',
            suboptions: [
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Consumption',
                component: 'FuelConsumptionPage'
              },
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Report',
                component: 'FuelConsumptionReportPage'
              },
              {
                iconName: 'custom-fuel',
                displayText: 'Fuel Chart',
                component: 'FuelChartPage'
              }
            ]
          });
          // Load options with nested items (with icons)
          // -----------------------------------------------
          this.options.push({
            displayText: 'Reports',
            iconName: 'arrow-dropright',
            suboptions: [
              {
                iconName: 'clipboard',
                displayText: 'AC Report',
                component: 'AcReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Daily Report',
                component: 'DailyReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Summary Report',
                component: 'DeviceSummaryRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Geofenceing Report',
                component: 'GeofenceReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Overspeed Report',
                component: 'OverSpeedRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Ignition Report',
                component: 'IgnitionReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Stoppage Report',
                component: 'StoppagesRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Distnace Report',
                component: 'DistanceReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Trip Report',
                component: 'TripReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Route Violation Report',
                component: 'RouteVoilationsPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'Speed Variation Report',
                component: 'SpeedRepoPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'SOS Report',
                component: 'SosReportPage'
              },
              {
                iconName: 'clipboard',
                displayText: 'POI Report',
                component: 'POIReportPage'
              }
            ]
          });

          // Load options with nested items (without icons)
          // -----------------------------------------------
          this.options.push({
            displayText: 'Routes',
            iconName: 'map',
            component: 'RoutePage'
          });

          // Load special options
          // -----------------------------------------------
          this.options.push({
            displayText: 'Customer Support',
            suboptions: [
              {
                iconName: 'star',
                displayText: 'Rate Us',
                component: 'FeedbackPage'
              },
              {
                iconName: 'mail',
                displayText: 'Contact Us',
                component: 'ContactUsPage'
              },
            ]
          });

          this.options.push({
            displayText: 'Profile',
            iconName: 'person',
            component: 'ProfilePage'
          });
        }
      }
    }

    console.log("check localstorage for is dealer value=> ", localStorage.getItem("isDealervalue"))
 
    var _DealerStat = localStorage.getItem("dealer_status");
    var _CustStat = localStorage.getItem("custumer_status");
    var onlyDeal = localStorage.getItem("OnlyDealer");

    if (_DealerStat != null || _CustStat != null) {
      if (_DealerStat == 'ON' && _CustStat == 'OFF') {
        this.options[2].displayText = 'Admin';
        this.options[2].iconName = 'person';
        this.options[2].component = 'DashboardPage';

        this.options[3].displayText = 'Customers';
        this.options[3].iconName = 'contacts';
        this.options[3].component = 'CustomersPage';
      } else {
        if (_DealerStat == 'OFF' && _CustStat == 'ON') {
          this.options[2].displayText = 'Dealers';
          this.options[2].iconName = 'person';
          this.options[2].component = 'DashboardPage';
        } else {
          if (_DealerStat == 'OFF' && _CustStat == 'OFF' && onlyDeal == null) {
            this.options[2].displayText = 'Dealers';
            this.options[2].iconName = 'person';
            this.options[2].component = 'DealerPage';

            this.options[3].displayText = 'Customers';
            this.options[3].iconName = 'contacts';
            this.options[3].component = 'CustomersPage';
          } else {
            if (onlyDeal == 'true') {
              this.options[2].displayText = 'Customers';
              this.options[2].iconName = 'contacts';
              this.options[2].component = 'CustomersPage';
            }
          }
        }
      }
    }

    this.events.subscribe("sidemenu:event", data => {
      console.log("sidemenu:event=> ", data);
      if (data) {
        this.options[2].displayText = 'Dealers';
        this.options[2].iconName = 'person';
        this.options[2].component = 'DashboardPage';
      }
    });

  }

  public onOptionSelected(option: SideMenuOption): void {

    this.menuCtrl.close().then(() => {
      if (option.custom && option.custom.isLogin) {
        this.presentAlert('You\'ve clicked the login option!');
      } else if (option.custom && option.custom.isLogout) {
        this.presentAlert('You\'ve clicked the logout option!');
      } else if (option.custom && option.custom.isExternalLink) {
        let url = option.custom.externalUrl;
        window.open(url, '_blank');
      } else {
        const params = option.custom && option.custom.param;
       
        if (option.displayText == 'Admin' && option.component == 'DashboardPage') {
          localStorage.setItem("dealer_status", 'OFF')
          localStorage.setItem('details', localStorage.getItem("superAdminData"));
          localStorage.removeItem('superAdminData');
        }

        if (option.displayText == 'Dealers' && option.component == 'DashboardPage') {
          if (localStorage.getItem('custumer_status') == 'ON') {

            var _dealdata = JSON.parse(localStorage.getItem("dealer"));

            if (localStorage.getItem("superAdminData") != null || this.islogin.isSuperAdmin == true) {
              localStorage.setItem("dealer_status", 'ON')
            } else {
              if (_dealdata.isSuperAdmin == true) {
                localStorage.setItem("dealer_status", 'OFF')
              } else {
                localStorage.setItem("OnlyDealer", "true");
              }
            }
            localStorage.setItem("custumer_status", 'OFF');
            localStorage.setItem('details', localStorage.getItem("dealer"));

          } else {
            console.log("something wrong!!")
          }
        }
        this.nav.setRoot(option.component, params);
      }
    });
  }

  public collapseMenuOptions(): void {
    this.sideMenu.collapseAllOptions();
  }

  public presentAlert(message: string): void {
    let alert = this.alertCtrl.create({
      title: 'Information',
      message: message,
      buttons: ['Ok']
    });
    alert.present();
  }

  openPage(page, index) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    if (page.component) {
      this.nav.setRoot(page.component);
      this.menuCtrl.close();
    } else {
      if (this.selectedMenu) {
        this.selectedMenu = 0;
      } else {
        this.selectedMenu = index;
      }
    }
  }

  chkCondition() {
    this.events.subscribe("event_sidemenu", data => {
      this.islogin = JSON.parse(data);
      this.options[2].displayText = 'Dealers';
      this.options[2].iconName = 'person';
      this.options[2].component = 'DashboardPage';
      this.initializeOptions();
    })
    this.initializeOptions();
  }
}

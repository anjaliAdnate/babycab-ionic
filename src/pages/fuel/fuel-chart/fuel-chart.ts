import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Toast, ToastController } from 'ionic-angular';
import { Chart } from 'chart.js';
import * as moment from 'moment';
import { ApiServiceProvider } from '../../../providers/api-service/api-service';

@IonicPage()
@Component({
  selector: 'page-fuel-chart',
  templateUrl: 'fuel-chart.html',
})
export class FuelChartPage implements OnInit {
  islogin: any;
  datetimeStart: any;
  datetimeEnd: any;
  portstemp: any[] = [];
  _vehId: any;
  @ViewChild('lineCanvas') lineCanvas;
  lineChart: any;
  series: any;
  lineChartData: any = [];
  lineChartLabels: any = [];

  constructor(
    public apiCall: ApiServiceProvider,
    public toastCtrl: ToastController,
    public navCtrl: NavController, public navParams: NavParams) {
    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("user details=> ", JSON.stringify(this.islogin));
    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FuelChartPage');
  }

  ngOnInit() {
    this.getdevices();
    this.lineChartData.push([]);
    this.series = ["Fuel in Litre"];
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apiCall.startLoading().present();
    this.apiCall.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apiCall.stopLoading();
        this.portstemp = data.devices;
      },
        err => {
          this.apiCall.stopLoading();
          console.log(err);
        });
  }

  getid(veh) {
    this._vehId = veh.Device_ID;
  }
  fuelGraphData = [];
  InsertionTime = [];
  getFueldata() {
    let that = this;
    that.InsertionTime = [];
    that.fuelGraphData = [];
    if (this._vehId == undefined) {
      let toast = this.toastCtrl.create({
        message: 'Please select the vehicle first.',
        duration: 1500,
        position: 'bottom'
      })
      toast.present();
      return
    }
    var url = "https://www.oneqlik.in/summary/fuel?i=" + this._vehId + "&f=" + new Date(that.datetimeStart).toISOString() + "&t=" + new Date(that.datetimeEnd).toISOString();
    this.apiCall.startLoading().present();
    this.apiCall.getSOSReportAPI(url)
      .subscribe(res => {
        this.apiCall.stopLoading();
        console.log("data fuel chart: ", res);
        let graphData = res;
        var fuelData = 0;
        for (var i = graphData.length - 1; i >= 0; i--) {
          if (graphData[i].currentFuel != undefined) {
            fuelData = parseInt(graphData[i].currentFuel);
          }
          this.fuelGraphData.push(fuelData);
          // fuelData = graphData[i].currentFuel ? graphData[i].currentFuel : 0;
          // if (fuelData != undefined) {
          //   this.fuelGraphData.push(fuelData);
          //   console.log(i + " : " + this.fuelGraphData)

          // } else {
          //   this.fuelGraphData.push(0);
          // }
          var iTime = new Date(graphData[i].insertionTime).toLocaleString();
          this.InsertionTime.push(iTime)
        }

        // const newArray = this.fuelGraphData.filter(value => !Number.isNaN(value));
        // console.log("newArray => "+ newArray)
        this.lineChartData[0] = (this.fuelGraphData);
        // console.log("lineChartData",this.lineChartData);
        this.lineChartLabels = this.InsertionTime;
        if (this.lineChart) {
          this.lineChart.destroy();
        }

        this.lineChart = new Chart(this.lineCanvas.nativeElement, {

          type: 'line',
          data: {
            labels: this.lineChartLabels,
            datasets: [{
              label: this.series,
              data: this.lineChartData[0],
              color: this.lineChartColors
            }]
          },
          options: {
            scales: {
              xAxes: [{
                stacked: true,
                ticks: {
                  //this will fix your problem with NaN
                  callback: function (label, index, labels) {
                    return label ? label : '';
                  }
                }
              }],
              yAxes: [{
                ticks: {
                  beginAtZero: true
                }
              }]
            }
          }
          // options: {
          //   responsive: true,
          //   scales: {
          //     yAxes: [{
          //       ticks: {
          //         beginAtZero: true,
          //       }
          //     }],

          //   }
          // }
        });
      },
        err => {
          this.apiCall.stopLoading();
          console.log("getting error: ", err)
        })
  }

  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    },
    { // dark grey
      backgroundColor: 'rgba(77,83,96,0.2)',
      borderColor: 'rgba(77,83,96,1)',
      pointBackgroundColor: 'rgba(77,83,96,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(77,83,96,1)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  public lineChartOptions: any = {
    //    labels:this.lineChartLabels,
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: true,
        }
      }],

    }

  };

  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }


}

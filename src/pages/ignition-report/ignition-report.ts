import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { ApiServiceProvider } from '../../providers/api-service/api-service';
import * as moment from 'moment';
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-ignition-report',
  templateUrl: 'ignition-report.html',
})
export class IgnitionReportPage implements OnInit {

  islogin: any;
  Ignitiondevice_id: any;
  igiReport: any[] = [];

  datetimeEnd: any;
  datetimeStart: string;
  devices: any;
  isdevice: string;
  portstemp: any;
  datetime: any;
  igiReportData: any[] = [];
  locationEndAddress: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public apicalligi: ApiServiceProvider,
    public toastCtrl: ToastController) {

    this.islogin = JSON.parse(localStorage.getItem('details')) || {};
    console.log("email => " + this.islogin._id);

    this.datetimeStart = moment({ hours: 0 }).format();
    this.datetimeEnd = moment().format();//new Date(a).toISOString();
  }

  ngOnInit() {
    this.getdevices();
  }

  getdevices() {
    var baseURLp = 'https://www.oneqlik.in/devices/getDeviceByUser?id=' + this.islogin._id + '&email=' + this.islogin.email;
    if (this.islogin.isSuperAdmin == true) {
      baseURLp += '&supAdmin=' + this.islogin._id;
    } else {
      if (this.islogin.isDealer == true) {
        baseURLp += '&dealer=' + this.islogin._id;
      }
    }
    this.apicalligi.startLoading().present();
    this.apicalligi.getdevicesForAllVehiclesApi(baseURLp)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.devices = data;
        this.portstemp = data.devices;
      },
        err => {
          this.apicalligi.stopLoading();
          console.log(err);
        });
  }

  getIgnitiondevice(selectedVehicle) {
    console.log("selectedVehicle=> ", selectedVehicle)
    this.Ignitiondevice_id = selectedVehicle.Device_Name;
  }

  getIgnitiondeviceReport() {

    if (this.Ignitiondevice_id == undefined) {
      this.Ignitiondevice_id = "";

    }
    let that = this;
    this.apicalligi.startLoading().present();

    this.apicalligi.getIgiApi(new Date(that.datetimeStart).toISOString(), new Date(that.datetimeEnd).toISOString(), this.Ignitiondevice_id, this.islogin._id)
      .subscribe(data => {
        this.apicalligi.stopLoading();
        this.igiReport = data;
      if(this.igiReport.length > 0) {
        this.innerFunc(this.igiReport);
      } else {
        let toast = this.toastCtrl.create({
          message: 'Report(s) not found for selected Dates/Vehicle.',
          duration: 1500,
          position: 'bottom'
        })
        toast.present();
      }
       

      }, error => {
        this.apicalligi.stopLoading();
        console.log(error);
      })
  }

  innerFunc(igiReport) {
    let outerthis = this;
    outerthis.igiReportData = [];
    outerthis.locationEndAddress = undefined;
    var i = 0, howManyTimes = igiReport.length;
    function f() {

      outerthis.igiReportData.push({
        'vehicleName': outerthis.igiReport[i].vehicleName,
        'switch': outerthis.igiReport[i].switch,
        'timestamp': outerthis.igiReport[i].timestamp
      });
      if (outerthis.igiReport[i].lat != null && outerthis.igiReport[i].long != null) {
        var latEnd = outerthis.igiReport[i].lat;
        var lngEnd = outerthis.igiReport[i].long;
        var latlng = new google.maps.LatLng(latEnd, lngEnd);

        var geocoder = new google.maps.Geocoder();

        var request = {
          latLng: latlng
        };
        geocoder.geocode(request, function (data, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (data[1] != null) {
              outerthis.locationEndAddress = data[1].formatted_address;
            }
          }
          outerthis.igiReportData[outerthis.igiReportData.length - 1].address = outerthis.locationEndAddress;
        })

      } else {
      
        outerthis.igiReportData[outerthis.igiReportData.length - 1].address = 'N/A';
      }
      console.log(outerthis.igiReportData);

      i++;
      if (i < howManyTimes) {
        setTimeout(f, 100);
      }

    }
    f();
  }


}
